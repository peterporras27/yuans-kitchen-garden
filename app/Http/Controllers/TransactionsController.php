<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Cart;
use Validator;

class TransactionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
        
        $this->validation = [
            'status' => 'required',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $trans = Transaction::find($id);

        if (!$trans) { return redirect('home'); }

        $carts = Cart::where('transaction_code','=',$trans->transaction_code)->get();

        $barangay = ["Aquino Nobleza West (Pob.)","Aquino Nobleza East (Pob.)","R. Armada (Pob.)","Concepcion Pob. (D.G. Abordo)","Golgota (Pob.)","Locsin (Pob.)","Don T. Lutero Center (Pob.)","Don T. Lutero East (Pob.)","Don T. Lutero West Pob. (Don T. Lutero North)","Crispin Salazar North (Pob.)","Crispin Salazar South (Pob.)","San Julian (Pob.)","San Pedro (Pob.)","Santa Rita (Pob.)","Capt. A. Tirador (Pob.)","S. M. Villa (Pob.)"];

        $fee = 200;
        if ($trans->province=='janiuay') {
            $fee = 100;
            if (in_array($trans->barangay, $barangay)) {
                $fee = 50;
            }
        }

        return view('admin.edit',compact('trans','carts','fee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $trans = Transaction::find($id);

        if (!$trans) 
        {
            return redirect('home')->with('error', 'Order no longer exist.');
        }

        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('home')->with('error', $validator->messages());
        }
        
        $trans->status = $request->input('status');
        
        if ($request->input('reason')) {
            $trans->reason = $request->input('reason');
        }

        $trans->save();

        $carts = Cart::where('transaction_code','=',$trans->transaction_code)->get();

        foreach ($carts as $cart) {
            $cart->status = $request->input('status');
            $cart->save();
        }

        return redirect('home')->with('success','Order succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
