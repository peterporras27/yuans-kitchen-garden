<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use Validator;


class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth','admin']);
        
        $this->validation = [
            'name' => 'required|unique:category|string|max:255',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $params['categories'] = Category::paginate(10);

        return view('admin.category.index',$params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('category')->with('error', $validator->messages());
        }

        $category = new Category;
        $category->fill( $request->all() );
        $category->save();

        return redirect('category')->with('success','Category succesfully added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $category = Category::find($id);

        if (!$category) 
        {
            return redirect('category')->with('error', 'Category no longer exist.');
        }

        // Input validation
        $validator = Validator::make( $request->all(), $this->validation );

        if( $validator->fails() ) 
        {
            return redirect('category')->with('error', $validator->messages());
        }
        
        $category->fill( $request->all() );
        $category->save();

        return redirect('category')->with('success','Category succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);

        if (!$category) 
        {
            return redirect('category')->with('error', 'Category no longer exist.');
        }

        $category->delete();

        return redirect('category')->with('success','Category succesfully removed!');

    }
}
