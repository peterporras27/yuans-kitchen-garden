<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Transaction;
use App\Product;
use App\Category;
use App\Images;
use App\Cart;
use Image;
use Storage;
use Auth;

class PublicController extends Controller
{
    public function __construct()
    {
        $this->barangay = ["Aquino Nobleza West (Pob.)","Aquino Nobleza East (Pob.)","R. Armada (Pob.)","Concepcion Pob. (D.G. Abordo)","Golgota (Pob.)","Locsin (Pob.)","Don T. Lutero Center (Pob.)","Don T. Lutero East (Pob.)","Don T. Lutero West Pob. (Don T. Lutero North)","Crispin Salazar North (Pob.)","Crispin Salazar South (Pob.)","San Julian (Pob.)","San Pedro (Pob.)","Santa Rita (Pob.)","Capt. A. Tirador (Pob.)","S. M. Villa (Pob.)"];
    }

    public function homepage()
    {
    	$products = Product::where('available','=', 1)->get();
    	$categories = Category::all();

    	return view('public.index',compact('products','categories'));
    }

    public function img($id)
    {
    	$img = Images::find($id);
    	return Image::make(Storage::get($img->filename))->response();
    }

    public function receipt($id)
    {
        $trans = Transaction::find($id);
        if (!$trans) { return redirect('/'); }
        if ($trans->status != 'delivered') { return redirect('/'); }

        $carts = Cart::where('transaction_code','=',$trans->transaction_code)->get();

        $fee = 200;
        if ($trans->province=='janiuay') {
            $fee = 100;
            if (in_array($trans->barangay, $this->barangay)) {
                $fee = 50;
            }
        }

        return view('public.receipt',compact('trans','carts','fee'));
    }

    public function order($id)
    {
    	$trans = Transaction::find($id);

    	if (!$trans) { return redirect('/'); }
    	if ( $trans->user_id != auth()->user()->id ) { return redirect('/');  }

    	$carts = Cart::where('transaction_code','=',$trans->transaction_code)->get();

        $fee = 200;
        if ($trans->province=='janiuay') {
            $fee = 100;
            if (in_array($trans->barangay, $this->barangay)) {
                $fee = 50;
            }
        }

    	return view('public.order',compact('trans','carts','fee'));
    }

    public function verify(Request $request)
    {
    	if ( Auth::check() ) {
    		
    		if (Auth::user()->verified_member) {
    			return redirect('/');	
    		}

    	} else {

			return redirect('/');    		
    	}

        $params = ['code'=>''];

        if ($request->input('code')) {
            $params['code'] = $request->input('code');
        }

    	return view('public.verify',$params);
    }

    public function verify_user(Request $request)
    {
    	if ( Auth::check() ) {

    		if ($request->input('code')==Auth::user()->code) 
    		{
    			Auth::user()->verified_member = 1;
    			Auth::user()->save();

                $carts = Cart::where([
                    ['user_id','=',auth()->user()->id],
                    ['status','=','hold']
                ])->get();

                if ( $carts->count()==0 ) {

                    return redirect('/')->with('success','Account Successfully Verified!');

                } else {

                    return redirect('/cart')->with('success','Account Successfully Verified!');
                }

    		} else {

    			return redirect('/verify')->with('error','Wrong verification code.');
    		}
    	}

    	return redirect('/verify')->with('error','Please try again.');
    }

    public function send_verification()
    {
        if ( Auth::check() ) {

            $data = array(
                'code' => Auth::user()->code,
                'name' => Auth::user()->first_name
            );

            Mail::send('mail', $data, function($message) {
                $message->to(Auth::user()->email, "Yuan's")->subject("Account Verification");
                $message->from('yuans@iloiloshop.com','Yuans');
            });

            return redirect('/verify')->with('success','Verification code was sent successfully to you your email.');  
        }

        return redirect('/verify');
    }

    public function add_cart(Request $request)
    {
    	$return = array(
    		'error' => true,
    		'message' => 'Please try again.',
    		'data' => null
    	);

    	if ( !Auth::check() ) {
    		
    		$return['message'] = 'Kindly login or register to order on our menu.<hr><a href="/login" class="btn btn-primary btn-sm">Login</a> | <a href="/register" class="btn btn-success btn-sm">Register</a>';
    		return $return;
    	}

        if ( Auth::user()->banned ) {
            $return['message'] = 'Your account has been banned and will no longer avail our services.';
            return $return;
        }

    	$id = $request->input('id');
    	$product = Product::find($id);

    	$cart = Cart::where([
    		['product_id', '=', $id],
    		['user_id', '=', auth()->user()->id],
            ['status', '=', 'hold']
    	])->first();

    	if ($cart) {

    		$cart->quantity = ($request->input('quantity')) ? $request->input('quantity') : 1;
    		$cart->save();

    	} else {

    		$cart = new Cart;
    		$cart->product_name = $product->name;
	        // $cart->transaction_code // To be filled up if checkout is met.
	        $cart->product_id = $product->id;
	        $cart->user_id = auth()->user()->id;
	        $cart->quantity = $request->input('quantity') ? $request->input('quantity'): 1;
	        $cart->price = $product->price;
	        $cart->save();
    	}

    	$return['count'] = auth()->user()->cart();
    	$return['error'] = false;
    	$return['message'] = 'Product successfully added to cart.';

    	return $return;
	}
}
