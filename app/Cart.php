<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $primaryKey = 'id';
	protected $table = 'carts';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name',
        'transaction_code',
        'product_id',
        'user_id',
        'quantity',
        'price',
        'status'
    ];
}
