@extends('homepage')
@section('header')
<style>
.description{margin-bottom: 15px;}
.cat{min-height: 300px;}
.tab-content{
	padding: 15px;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;

    border-bottom-right-radius: 5px;
    border-bottom-left-radius: 5px;
    border-top-right-radius: 5px;
}
</style>
@endsection
@section('content')

<div class="mainTitle">
	<div class="container" align="center">
		<h1>Cart Order Information</h1>
	</div>
</div>

<div class="container marketing">

	<div role="tabpanel">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active">
				<a href="#home" aria-controls="home" role="tab" data-toggle="tab" style="color: #000;">Checkout Information</a>
			</li>
			{{-- <li role="presentation">
				<a id="checkout" href="#tab" aria-controls="tab" role="tab" data-toggle="tab" style="color: #000;">Checkout</a>
			</li> --}}
		</ul>
	
		<!-- Tab panes -->
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">

				<form id="checkout-form" action="{{ route('cart.store') }}" method="POST" role="form">
					@csrf
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label>First Name:</label>
								<input type="text" name="first_name" class="form-control" value="{{ old('first_name') ? old('first_name') : auth()->user()->first_name }}" required>
							</div>
							<div class="form-group">
								<label>Last Name:</label>
								<input type="text" name="last_name" class="form-control" value="{{ old('last_name') ? old('last_name') : auth()->user()->last_name }}" required>
							</div>
							<div class="form-group">
								<label>Phone:</label>
								<input type="text" name="phone" class="form-control" value="{{ old('phone') ? old('phone') : auth()->user()->phone }}" required>
							</div>
							<div class="form-group">
								<label>Order Notes:</label>
								<textarea rows="3" name="notes" class="form-control" placeholder="Special requests etc."></textarea>
							</div>
						</div>
						<div class="col-md-6">
							 
							<input type="hidden" name="address" class="form-control" value="{{ old('address') ? old('address') : auth()->user()->address }}" required>
							
							<div class="form-group">
								<label>Town:</label>
								<select id="province" name="province" class="form-control">
									<option value="janiuay">Janiuay</option>
									<option value="badiangan">Badiangan</option>
									<option value="mina">Mina</option>
									<option value="lambunao">Lambunao</option>
									<option value="cabatuan">Cabatuan</option>
									<option value="iloilo">Iloilo City (Capital)</option>
								</select>
							</div>
							<div class="form-group">
								<label>Barangay:</label>
								<select id="barangay" name="barangay" class="form-control"></select>
							</div>
							<div class="form-group">
								<label>Address:</label>
								<input id="address" type="text" value="" class="form-control" disabled>
							</div>
						</div>
					</div>
				</form>
				<hr>
				<div class="table-responsive">
					<table class="table table-hover table-bordered">
						<thead>
							<tr class="info">
								<th>Option</th>
								<th>Name</th>
								<th>Quantity</th>
								<th>Price</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<?php $total = 0; $count = 0; ?>
							@foreach($carts as $cart)
								<tr>
									<td>
										<form action="{{ route('cart.destroy', $cart->id) }}" method="POST" style="display:inline;">
                                            @csrf
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger btn-xs">Remove <i class="glyphicon glyphicon-remove"></i></button>
                                        </form>
									</td>
									<td>{{ $cart->product_name }}</td>
									<td>
										<input onchange="cartUpdate(this,{{$cart->product_id}})" data-price="{{ $cart->price }}" type="number" min="1" value="{{ $cart->quantity }}">
									</td>
									<td>₱{{ number_format($cart->price,2,'.',',') }}</td>
									<td id="total-{{$cart->product_id}}">₱{{ number_format(($cart->price*$cart->quantity),2,'.',',') }}</td>
								</tr>
								<?php 
								$prodprice = $cart->price*$cart->quantity;
								$total = $total+$prodprice; 
								$count = $count+$cart->quantity; ?>
							@endforeach	
							<tr>
								<td><b>Delivery Fee</b></td>
								<td></td>
								<td></td>
								<td></td>
								<td><span id="fee">₱50.00</span></td>
							</tr>
							<tr class="success">
								<td><b>TOTAL:</b></td>
								<td></td>
								<td class="quantity">{{$count}}</td>
								<td></td>
								<td><b class="grand">₱{{ number_format($total,2,'.',',') }}</b></td>
							</tr>
						</tbody>
					</table>
					<div class="alert alert-info">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<strong>Note:</strong> No cancelation or changes allowed. If the order was not accepted or if cancelled, account will automatically be blocked on our online delivery system. 
					</div>
					{{-- <a href="#tab" class="btn btn-primary" onclick="jQuery('#checkout').click();">Next Step <i class="glyphicon glyphicon-chevron-right"></i></a> --}}
				</div>
				<button onclick="jQuery('#checkout-form').submit();" class="btn btn-success">Request For Delivery <i class="glyphicon glyphicon-ok"></i></button>
			
			</div>
			<div role="tabpanel" class="tab-pane" id="tab" align="left">

			</div>
		</div>
	</div>


</div>

@endsection
@section('footer')
<script>
var $=jQuery;
var overall = {{ number_format($total,2,'.','') }};
var delivery_fee = 50;

function cartUpdate(input,id){

	var quantity = parseInt($(input).val());
	var price = $(input).data('price');

	var formatter = new Intl.NumberFormat('en-PH', {
	  style: 'currency',
	  currency: 'PHP',

	  // These options are needed to round to whole numbers if that's what you want.
	  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	});

	$('#total-'+id).html( formatter.format((parseFloat(price)*quantity)) );

	$.ajax({
		url: '{{route('add')}}',
		type: 'POST',
		dataType: 'json',
		data: {id:id,_token:'{{csrf_token()}}',quantity:quantity},
	}).always(function(res) {

		var total = 0;
		var grandTotal = 0;
		$('[type="number"]').each(function(index, el) {
			var quantity = parseInt($(this).val());
			var price = $(this).data('price');
			total = parseInt(total)+quantity;
			grandTotal = grandTotal+(parseFloat(price)*quantity);
		});

		$('.quantity').text(total);
		overall = grandTotal;
		grandTotal = parseFloat(delivery_fee)+parseFloat(grandTotal);
		$('.grand').text(formatter.format(grandTotal));
		if (res.error) {

			toastr.warning('Oops! something went wrong.', 'Error!');

		} else {

			toastr.success('Cart Updated!', 'Success!');
		}
	});
}

var janiuay = ["Aquino Nobleza West (Pob.)","Panuran","Pararinga","Patong-patong","Quipot","Santo Tomas","Sarawag","Tambal","Tamu-an","Tiringanan","Tolarucan","Tuburan","Ubian","Yabon","Aquino Nobleza East (Pob.)","Pangilihan","R. Armada (Pob.)","Concepcion Pob. (D.G. Abordo)","Golgota (Pob.)","Locsin (Pob.)","Don T. Lutero Center (Pob.)","Don T. Lutero East (Pob.)","Don T. Lutero West Pob. (Don T. Lutero North)","Crispin Salazar North (Pob.)","Crispin Salazar South (Pob.)","San Julian (Pob.)","San Pedro (Pob.)","Santa Rita (Pob.)","Capt. A. Tirador (Pob.)","S. M. Villa (Pob.)","Carigangan","Agcarope","Aglobong","Aguingay","Anhawan","Atimonan","Balanac","Barasalon","Bongol","Cabantog","Calmay","Canawili","Canawillian","Caranas","Caraudan","Abangay","Cunsad","Dabong","Damires","Damo-ong","Danao","Gines","Guadalupe","Jibolo","Kuyot","Madong","Manacabac","Mangil","Matag-ub","Monte-Magapa"];
var badiangan = ["Mainguit","Bingauan","Tina","Teneclan","Tamocol","Talaba","Sinuagan","Sianon","Sariri","San Julian","Poblacion (Badiangan)","Odiongan","Mapili Sanjo","Mapili Grande","Manaolan","Malublub","Agusipan","Linayuan","Latawan","Iniligan","Indorohan","Ilongbukid","Guinawahan","Catubig","Calansanan","Cabayogan","Cabanga-an","Budiawe","Botong","Bita-oyan","Astorga"];
var mina = ["Mina East (Pob.)","Yugot","Tumay","Tolarucan","Tipolo","Talibong Pequeño","Talibong Grande","Singay","Naumuan","Nasirum","Mina West (Pob.)","Abat","Janipa-an East","Janipa-an West","Guibuangan","Dala","Capul-an","Cabalabaguan","Bangac","Badiangan","Amiroy","Agmanaphao"];
var lambunao = ["Malag-it","Pasig","Panuran","Pandan","Pajo","Natividad","Misi","Marong","Maribong","Manaulan","Patag","Maite Pequeño","Maite Grande","Magbato","Madarag","Lumanay","Legayada","Lanot Pequeño","Lanot Grande","Sibacungan","Walang","Tuburan","Tubungan","Tranghawan","Tampucao","Supoc","Simsiman","Sibaguan","Jorog","San Gregorio","Sagcup","Quiling","Pungsod","Pughanan","Poong","Poblacion Ilaya","Poblacion Ilawod","Binaba-an Armada","Burirao","Buri","Bontoc","Bonbon","Binaba-an Tirador","Binaba-an Portigo","Binaba-an Limoso","Binaba-an Labayno","Buwang","Bayuco","Bansag","Banban","Balagiao","Bogongbong","Badiangan","Alugmawa","Agtuman","Cayan Oeste","Jayubo","Hipgos","Gines","Daanbanwa","Cunarum","Cubay","Coto","Corot-on","Agsirab","Cayan Este","Capangyan","Caninguan","Caloy-Ahan","Caguisanan","Cabunlawan","Cabugao","Cabatangan"];
var cabatuan = ["Pamuringao Garrido","Zone VI Pob. (Barangay 6 )","Zone V Pob. (Barangay 5)","Zone IV Pob. (Barangay 4)","Zone III Pob. (Barangay 3)","Zone II Pob. (Barangay 2)","Zone XI Pob. (Barangay 11)","Zone X Pob. (Barangay 10)","Zone I Pob. (Barangay 1)","Zone VII Pob. (Barangay 7)","Pamuringao Proper","Pamul-Ogan","Pagotpot","Pacatin","Morubuan","Maraguit","Manguna","Tacdangan","Tuy-an","Tupol Oeste","Tupol Este","Tupol Central","Tiring","Tinio-an","Tigbauan Road","Talanghauan","Lutac","Tabucan","Sulanga","Salacay","Puyas","Pungtod","Zone IX Pob. (Barangay 9)","Zone VIII Pob. (Barangay 8)","Baluyan","Gaub","Duyanduyan","Calayo","Calawagan","Cagban","Cadoldolan","Bulay","Banguit","Gines Interior","Balabag","Bacan","Ayong","Ayaman","Anuang","Amurao","Amerang","Ito Sur","Leong","Lag-an","Jelicuon Montinola","Jelicuon Lusaya","Janipaan Olo","Janipaan Oeste","Janipaan Este","Janipaan Central","Acao","Ito Norte","Ingas","Inaladan","Inaca","Inabasan","Guibuangan Tigbauan","Gines Patag"];
var iloilo = ["Rizal Estanzuela","Rizal Palapala I","Rizal Palapala II","PHHC Block 17","PHHC Block 22 NHA","Poblacion Molo","President Roxas","Progreso-Lapuz","Punong-Lapuz","Quezon","Quintin Salas","Rima-Rizal","Our Lady Of Lourdes","Rizal Ibarra","Railway","Roxas Village","Sambag","Sampaguita","San Agustin","San Antonio","San Felix","San Isidro (Jaro)","Hibao-an Norte","Muelle Loney-Montes","Magsaysay","Magsaysay Village","Malipayon-Delgado","Mansaya-Lapuz","Marcelo H. del Pilar","Maria Clara","Maria Cristina","Mohon","Molo Boulevard","Montinola","San Jose (Jaro)","Nabitasan","Navais","Nonoy","North Fundidor","North Baluarte","North San Jose","Oñate de Leon","Obrero-Lapuz","Ortiz","Osmeña","West Timawa","Abeto Mirasol Taft South (Quirino Abeto)","Tagbac","Tap-oc","Taytay Zone II","Ticud (La Paz)","Timawa Tanza I","Timawa Tanza II","Ungka","Veterans Village","Villa Anita","West Habog-habog","Tacas","Yulo-Arroyo","Yulo Drive","Zamora-Melliza","Pale Benedicto Rizal (Mandurriao)","Kahirupan","Luna (La Paz)","San Isidro (La Paz)","San Jose (City Proper)","Tabuc Suba (La Paz)","Rizal (La Paz)","Santo Domingo","San Jose (Arevalo)","San Juan","San Nicolas","San Pedro (Molo)","San Pedro (Jaro)","San Rafael","San Roque","San Vicente","Santa Filomena","Santa Rosa","Magdalo","Santo Niño Norte","Santo Niño Sur","Santo Rosario-Duran","Simon Ledesma","So-oc","South Baluarte","South San Jose","Taal","Tabuc Suba (Jaro)","Tabucan","Cuartero","Buntatala","Seminario (Burgos Jalandoni)","Caingin","Calahunan","Calaparan","Calumpang","Camalig","El 98 Castilla (Claudio Lopez)","Cochero","Compania","Concepcion-Montes","Buhang Taft North","Cubay","Danao","Mabolo-Delgado","Democracia","Desamparados","Divinagracia","Don Esteban-Lapuz","Dulonan","Dungon","Dungon A","Sinikway (Bangkerohan Lapuz)","Aguinaldo","Airport (Tabucan Airport)","Alalasan Lapuz","Arguelles","Arsenal Aduana","North Avanceña","Bakhaw","Balabago","Balantang","Baldoza","Dungon B","Bantud","Banuyao","Baybay Tanza","Benedicto (Jaro)","Bito-on","Monica Blumentritt","Bolilao","Bonifacio Tanza","Bonifacio (Arevalo)","Buhang","Liberation","Jereos","Calubihan","Kasingkasing","Katilingban","Kauswagan","Our Lady Of Fatima","Laguda","Lanit","Lapuz Norte","Lapuz Sur","Legaspi dela Rama","Javellana","Libertad, Santa Isabel","Libertad-Lapuz","Lopez Jaena (Jaro)","Loboc-Lapuz","Lopez Jaena Norte","Lopez Jaena Sur","Luna (Jaro)","M. V. Hechanova","Burgos-Mabini-Plaza","Macarthur","Guzman-Jesena","East Baluarte","East Timawa","Edganzon","Tanza-Esperanza","Fajardo","Flores","South Fundidor","General Hughes-Montes","Gloria","Gustilo","Santa Cruz","Habog-habog Salvacion","Hibao-an Sur","Hinactacan","Hipodromo","Inday","Infante","Ingore","Jalandoni Estate-Lapuz","Jalandoni-Wilson","Delgado-Jalandoni-Bagumbayan"];
var onehundred = ["Aquino Nobleza West (Pob.)","Aquino Nobleza East (Pob.)","R. Armada (Pob.)","Concepcion Pob. (D.G. Abordo)","Golgota (Pob.)","Locsin (Pob.)","Don T. Lutero Center (Pob.)","Don T. Lutero East (Pob.)","Don T. Lutero West Pob. (Don T. Lutero North)","Crispin Salazar North (Pob.)","Crispin Salazar South (Pob.)","San Julian (Pob.)","San Pedro (Pob.)","Santa Rita (Pob.)","Capt. A. Tirador (Pob.)","S. M. Villa (Pob.)"];

function totalallPrices()
{
	var formatter = new Intl.NumberFormat('en-PH', {
	  style: 'currency',
	  currency: 'PHP',

	  // These options are needed to round to whole numbers if that's what you want.
	  //minimumFractionDigits: 0, // (this suffices for whole numbers, but will print 2500.10 as $2,500.1)
	  //maximumFractionDigits: 0, // (causes 2500.99 to be printed as $2,501)
	});

	var total = parseFloat(overall)+parseFloat(delivery_fee);
	$('.grand').text( formatter.format(total) );
	$('#fee').text(formatter.format(delivery_fee));
}

function getAddress()
{
	var brgy = $('#barangay').val();
	var town = $('#province').val();

	town = town.toLowerCase().replace(/\b[a-z]/g, function(letter) {
	    return letter.toUpperCase();
	});

	if (town=='Iloilo') { town = ''; } else { town = town+','; }

	var addr = 'Brgy. '+brgy+', '+town+' Iloilo City';

	$('[name="address"]').val(addr);
	$('#address').val(addr);
}

$(document).ready(function() {

	for (var i = 0; i < janiuay.length; i++) {
		$('#barangay').append('<option value="'+janiuay[i]+'">'+janiuay[i]+'</option>');
	}

	totalallPrices();
	getAddress();

	$('#barangay').change(function () {

		var val = $(this).val();

		delivery_fee = 200;

		if ($('#province').val()=='janiuay') {

			delivery_fee = 50;

			if (onehundred.indexOf(val) < 0) {

				delivery_fee = 100;
			} 
		}

		totalallPrices();
		getAddress();
	});

	$('#province').change(function(event) {
		var province = $(this).val();
		var data = [];

		switch(province){
			case 'janiuay': data = janiuay; break;
			case 'badiangan': data = badiangan; break;
			case 'mina': data = mina; break;
			case 'lambunao': data = lambunao; break;
			case 'cabatuan': data = cabatuan; break;
			case 'iloilo': data = iloilo; break;
		}

		$('#barangay').html('');
		for (var i = 0; i < data.length; i++) {
			$('#barangay').append('<option value="'+data[i]+'">'+data[i]+'</option>');
		}

		if (province=='janiuay') {
			
			delivery_fee = 50;

		} else {

			delivery_fee = 200;
		}

		totalallPrices();
		getAddress();
	});
});
</script>
@endsection