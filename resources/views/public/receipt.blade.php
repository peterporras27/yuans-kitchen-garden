<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>Yuan's Receipt</title>
		<style>
			body {
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				text-align: center;
				color: #777;
			}

			body h1 {
				font-weight: 300;
				margin-bottom: 0px;
				padding-bottom: 0px;
				color: #000;
			}

			body h3 {
				font-weight: 300;
				margin-top: 10px;
				margin-bottom: 20px;
				font-style: italic;
				color: #555;
			}

			body a {
				color: #06f;
			}

			.invoice-box {
				max-width: 800px;
				margin: auto;
				padding: 30px;
				border: 1px solid #eee;
				box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
				font-size: 16px;
				line-height: 24px;
				font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
				color: #555;
			}

			.invoice-box table {
				width: 100%;
				line-height: inherit;
				text-align: left;
				border-collapse: collapse;
			}

			.invoice-box table td {
				padding: 5px;
				vertical-align: top;
			}

			.invoice-box table tr td:nth-child(2),
			.invoice-box table tr td:nth-child(3) {
				text-align: center;
			}

			.invoice-box table tr.top table td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.top table td.title {
				font-size: 45px;
				line-height: 45px;
				color: #333;
			}

			.invoice-box table tr.information table td {
				padding-bottom: 40px;
			}

			.invoice-box table tr.heading td {
				background: #eee;
				border-bottom: 1px solid #ddd;
				font-weight: bold;
			}

			.invoice-box table tr.details td {
				padding-bottom: 20px;
			}

			.invoice-box table tr.item td {
				border-bottom: 1px solid #eee;
			}

			.invoice-box table tr.item.last td {
				border-bottom: none;
			}

			.invoice-box table tr.total td:nth-child(4) {
				border-top: 2px solid #eee;
				font-weight: bold;
			}

			@media only screen and (max-width: 600px) {
				.invoice-box table tr.top table td {
					width: 100%;
					display: block;
					text-align: center;
				}

				.invoice-box table tr.information table td {
					width: 100%;
					display: block;
					text-align: center;
				}
			}
			.tar{text-align: right;}
			.tac{text-align: center;}
			.tal{text-align: center;}
			@media print {
			  /* style sheet for print goes here */
			  .noprint {
			    visibility: hidden;
			  }
			}
		</style>
	</head>

	<body>
		<h1>Yuan's Kitchen Garden</h1>
		<h3>Capture Beautiful Moments At Yuan's!</h3>
		<div class="invoice-box">
			<table>
				<tr class="top">
					<td class="title">
						<h2 style="margin:0;">Yuan's Kitchen Garden</h2>
						<small>Capture Beautiful Moments At Yuan's!</small>
					</td>
					<td></td>
					<td></td>
					<td class="tar">
						Receipt #: {{ strtoupper($trans->transaction_code) }}<br />
						Created: {{ $trans->created_at->format('j d, Y') }}<br />
						Due: {{ $trans->updated_at->format('j d, Y') }}
					</td>
				</tr>

				<tr class="information">
					<td>
						{{ $trans->address }}<br />
					</td>
					<td></td>
					<td></td>
					<td class="tar">
						{{ $trans->first_name.' '.$trans->last_name }}<br />
						{{ $trans->phone }}<br />
						{{ $trans->email }}
					</td>
				</tr>

				<tr class="heading">
					<td>Item</td>
					<td>Quantity</td>
					<td class="tac">Price</td>
					<td class="tar">Total</td>
				</tr>

				<?php $total = 0; $count = 0; ?>
				@foreach($carts as $cart)
					<tr class="item">
						<td>{{ $cart->product_name }}</td>
						<td class="tac">{{ $cart->quantity }}</td>
						<td class="tac">₱{{ number_format($cart->price,2,'.',',') }}</td>
						<td class="tar">₱{{ number_format( ($cart->price*$cart->quantity) ,2,'.',',') }}</td>
					</tr>
					<?php $total = $total+$cart->price; $count = $count+$cart->quantity; ?>
				@endforeach	
				<tr class="item last">
					<td>Delivery Fee</td>
					<td></td>
					<td></td>
					<td class="tar">₱{{ number_format( $fee,2,'.',',') }}</td>
				</tr>
				<tr class="total">
					<td><b>TOTAL:</b></td>
					<td class="tac">{{$count}}</td>
					<td></td>
					<td class="tar"><b class="grand">₱{{ number_format(($total+$fee),2,'.',',') }}</b></td>
				</tr>
			</table>
		</div>

		<div class="noprint" align="center">
		<button onclick="window.print();" style="padding: 10px 30px;margin-top: 15px;">Print Reciept</button>
		</div>
	</body>
</html>
