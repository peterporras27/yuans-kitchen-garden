@extends('admin.index')
@section('styles')
<style>
.gallery .thumbnail a{
	position: absolute;
    right: 5px;
    color: #000;
    background: #fff;
    border: 1px solid #eee;
    border-radius: 50%;
    padding: 5px;
    top: -15px;
    text-decoration: none;
}
.gallery .thumbnail a:hover{
	color: red;
}
</style>
@endsection
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Edit Product</h3>
				</div>
				<div class="panel-body">
				
					<form enctype="multipart/form-data" action="{{ route('products.update',$product->id) }}" method="POST" role="form">
					@csrf
					<input type="hidden" name="_method" value="PUT">
						<div class="col-md-6">
							<div class="form-group">
								<label for="product-name">Name</label>
								<input value="{{ $product->name }}" type="text" name="name" class="form-control" id="product-name" placeholder="">
							</div>

							<div class="form-group">
								<label for="product-description">Description</label>
								<textarea rows="10" name="description" class="form-control" id="product-description">{{ $product->description }}</textarea>
							</div>

							<button type="submit" class="btn btn-primary">Save <i class="fa fa-save"></i></button>

						</div>
						<div class="col-md-6">

							<div class="row">
								<div class="col-md-6 col-sm-6 col-xs-12">
									<div class="form-group">
										<label for="product-price">Price</label>
										<div class="input-group">
										  <span class="input-group-addon" id="product-price">₱</span>
										  <input value="{{ $product->price }}" type="text" class="form-control" name="price" placeholder="00.00" aria-describedby="product-price">
										</div>
									</div>

									<div class="form-group">
										<label for="product-available">Product Availability</label><br>
										<input type="checkbox" name="available" class="form-control js-switch" value="1" id="product-available" {{ ($product->available) ? 'checked':'' }}> 
										<span class="avail badge badge-pill badge-info">{{ ($product->available) ? 'Available':'Out of Stock' }}</span>
									</div>
								</div>
								<div class="col-md-6 col-sm-6 col-xs-12">
									@if($categories->count())
									<div class="form-group">
										<label for="product-category">Category</label>
										<select name="category_id" class="form-control" id="product-category">
											@foreach($categories as $cat)
											<option value="{{ $cat->id }}"{{ ($product->category_id == $cat->id) ? ' selected':'' }}>{{ $cat->name }}</option>
											@endforeach
										</select>
									</div>
									@endif	
								</div>
							</div>
							
							<div class="gallery row">
								@foreach($images as $img)
								<div class="col-md-3 col-sm-3 col-xs-6">
									<div class="thumbnail" style="width:100%;margin-bottom:15px;height:150px;background:url('{{ route('image',$img->id) }}') no-repeat center center;background-size:cover;">
										<a href="#" onclick="remove(this)" data-id="{{$img->id}}" class="fa fa-close"></a>
									</div>
								</div>
								@endforeach
							</div>
							<div align="center">
								<i id="imgspin" style="display: none;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
							</div>
							<br>
							<div class="form-group">
								<label for="product-photo">Select Photos</label>
								<input type="file" multiple name="photos[]" class="form-control" id="product-photo" placeholder="Select Image">
							</div>
							
						</div>
					
					</form>
				

				</div><!-- panel-body -->
			</div><!-- panel -->


		</div>
	</div>
</div>
@endsection
@section('scripts')
<script>
var $=jQuery;

function remove(btn)
{
	var id = $(btn).data('id');
	$.ajax({
		url: '/img/'+id+'/delete',
		type: 'GET',
		dataType: 'json',
		data: {id: id},
	})
	.always(function(res) {

		if (res.error) {

			alert(res.message);

		} else {

			$(btn).parent().parent().remove();
		}
	});
}
// Multiple images preview in browser
var imagesPreview = function(input, placeToInsertImagePreview) 
{
    if (input.files) 
    {
        var filesAmount = input.files.length;
        for (i = 0; i < filesAmount; i++) 
        {
            var reader = new FileReader();
            reader.onload = function(event) {
            	var r = '<div class="col-md-3 col-sm-3 col-xs-6"><div class="thumbnail" style="width:100%;margin-bottom:15px;height:150px;background:url('+event.target.result+') no-repeat center center;background-size:cover;"></div></div>';
                $($.parseHTML(r)).appendTo(placeToInsertImagePreview);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
};

$('#product-photo').on('change', function() {
	$('.gallery').html('');
    imagesPreview(this, 'div.gallery');
});

var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
elems.forEach(function(html) {
  var switchery = new Switchery(html);
});

var changeCheckbox = document.querySelector('.js-switch');
changeCheckbox.onchange = function() {
	if (changeCheckbox.checked) {
		$('.avail').text('Available');
		$('#product-available').val(1);
	} else {
		$('.avail').text('Out of stock');
		$('#product-available').val(0);
	}
};

$(document).ready(function() {
	$('#product-description').summernote({
		height: 300,                 // set editor height
		minHeight: null,             // set minimum height of editor
		maxHeight: null,             // set maximum height of editor
	});	
});
</script>
@endsection