<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Yuan's Kitchen Garden</title>
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
        <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/switchery.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/toastr.min.css')}}" rel="stylesheet">
        <link href="{{asset('summernote/summernote.min.css')}}" rel="stylesheet">
        @yield('styles')
    </head>
    <body>
        <nav class="navbar navbar-inverse" style="border-radius: 0;">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">Yuan's Kitchen Garden</a>
                <ul class="nav navbar-nav">
                    <li @if(Route::current()->getName() == 'admin') class="active" @endif>
                        <a href="{{ route('admin') }}">Orders</a>
                    </li>
                    <li @if(Route::current()->getName() == 'products.index') class="active" @endif>
                        <a href="{{ route('products.index') }}">Products</a>
                    </li>
                    <li @if(Route::current()->getName() == 'category.index') class="active" @endif>
                        <a href="{{ route('category.index') }}">Categories</a>
                    </li>
                    <li @if(Route::current()->getName() == 'users.index') class="active" @endif>
                        <a href="{{ route('users.index') }}">Users</a>
                    </li>
                    <li><a href="{{ route('users.edit', auth()->user()->id) }}">Settings</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
        @yield('title')
        @yield('content')

        <script src="{{asset('js/jquery-3.4.1.min.js')}}"></script>
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
        <script src="{{asset('js/switchery.min.js')}}"></script>
        <script src="{{asset('js/toastr.min.js')}}"></script>
        <script src="{{asset('summernote/summernote.min.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>
        @yield('scripts')
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
        @foreach ( $errors->all() as $message )
            <script>toastr.warning('{{ $message }}', 'Error!')</script>
        @endforeach
        @if(session('success'))
            <script>toastr.success('{{ session('success') }}', 'Success!')</script>
        @endif
        @if(session('error'))
            <script>toastr.error('{{ session('error') }}', 'Error!')</script>
        @endif
    </body>
</html>