<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PublicController@homepage')->name('home');
Route::get('img/{id}', 'PublicController@img')->name('image');
Route::get('verify', 'PublicController@verify')->name('verify');
Route::get('order/{id}', 'PublicController@order')->name('order');
Route::get('send-code', 'PublicController@send_verification')->name('send_code');
Route::get('receipt/{id}', 'PublicController@receipt')->name('receipt');

Route::post('add', 'PublicController@add_cart')->name('add');
Route::post('verify-user', 'PublicController@verify_user')->name('verify_user');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('admin');
Route::get('/img/{id}/delete', 'HomeController@deleteImage')->name('delete-img');

Route::resources([
    'settings' => 'SettingsController',
    'transaction' => 'TransactionsController',
    'products' => 'ProductsController',
    'category' => 'CategoryController',
    'users' => 'UsersController',
    'orders' => 'OrdersController',
    'cart' => 'CartController',
]);