<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'administrator',
            'email' => 'admin@yuans.com',
            'password' => Hash::make('secret'),
            'role' => 'admin'
        ]);
    }
}
