<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert([
            ['name' => 'Beef For I Let You Go, Try These!'],
            ['name' => 'Fish Be With You!'],
            ['name' => 'Yuan\'s Pizza Special'],
            ['name' => 'Yuan\'s Pasta'],
            ['name' => 'Chick\' En Eat Me'],
            ['name' => 'Yuan\'s Short Orders'],
            ['name' => 'Cocktail Drinks'],
            ['name' => 'Carafe Coolers'],
            ['name' => 'Veggy Good'],
            ['name' => 'Rice'],
            ['name' => 'Specialties'],
            ['name' => 'Ice Cream']
        ]);

    }
}
